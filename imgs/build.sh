#!/usr/bin/env sh

build_container() {
  local image=$1
  local platform=$2
  local ltag=$3
  local stag=$4

  - $platform build -t $stag -t $ltag ./$img/
  - $platform push "$stag"
  - $platform push "$ltag"
}

build() {
  local platform=$1

  local img=$2

  local stag=$3
  if [ -z $5 ]; then
    local ltag="latest"
  else
    local ltag=$5
  fi

  local repo=$4

  local tag1=$repo:$img-$stag
  local tag2=$repo:$img-$ltag

  $platform build -t $tag1 -t $tag2 -f $(dirname "$0")/$img/Dockerfile .
  $platform push $tag1
  $platform push $tag2
}

build $1 $2 $3 $4
