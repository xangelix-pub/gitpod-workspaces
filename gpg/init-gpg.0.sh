#!/usr/bin/env bash

echo "INFO: Setting SSH private key permissions."
sudo chmod 600 /home/gitpod/.ssh/id_rsa


until echo "INFO: Please enter your SSH key password:" && ssh-keygen -y -f ~/.ssh/id_rsa > /home/gitpod/.ssh/id_rsa.pub
do
  echo "FAIL: SSH Key Password Entry Failed"
  echo "Retrying..."
done

$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/init-gpg.1.sh
