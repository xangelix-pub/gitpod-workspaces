#!/usr/bin/env bash

until echo "INFO: Please enter your PGP key password:" && gpg --pinentry-mode=loopback --sign temp.txt
do
  echo "FAIL: PGP Password Entry #2 Failed"
  echo "Retrying..."
done

rm -f temp.txt temp.txt.gpg
echo "DONE: GPG initialization workaround complete"
